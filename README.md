![desktop](https://i.imgur.com/Rcuj4rX.jpg)
![wallpaper](https://i.imgur.com/jf3awdN.jpg)
This is my small dotfiles collection. For the sake of 
those who 
come after me, including myself in the future, this has some 
more in-depth instructions than I could find anywhere when I was 
doing all this garbage myself, in no particular order.

***BEFORE YOU LOSE YOUR MIND LIKE I DID GO THROUGH EVERY FILE 
AND MAKE SURE IT IS PROPERLY FORMATTED AND ALL THE DIRECTORIES 
ARE PROPER***

To this end, I'll list the files that I've uploaded and how 
they're referenced in location.

-	Scripts (shell commands which I should really just 
alias) are located in `~/Code/sh`. These are linked to 
`/usr/bin` so as to make them executable from any location.

-	`yaourt oh my zsh` will get you zsh, and zsh itself 
should 
already be located at `/bin/zsh/`, if not you should be able to 
install it from your package manager.

-	In order for zsh to not get really ugly, you need to get 
nerd 
fonts, available with `yaourt nerd fonts`. It's the big 
collection of fonts, you can't miss it.

-	I am working off an edited + copied zsh config (it was 
very 
close to what I already had in mind and I did a bunch to it, 
ok), so I'm not entirely certain what all the powerlevel9k 
options are. The wiki at https://github.com/bhilburn/powerlevel9k/wiki 
is fantastic, so it should be easy to get through.

-	I had lots of problems with fonts being mis-spaced, these are 
generally solved by reinstalling the font. You don't need to 
uninstall it, either, just `pacman -S <font>` or `yaourt 
<font>`.

-	`sudo pacman -S i3wm` gets you the base i3 stuff, `yaourt 
i3-gaps` will get you the gaps.i3's user guide is reasonably useful 
as a source, but is much easier to understand if you already know 
what the hell's going on. Read i3/config to get an idea of 
what's happening.

-	If you've never done this before and are having a hard time,
don't give up! There's actually not that much involved, 
especially when using someone else's dotfiles. This becomes 
immediately apparent once you give the `--exec_always feh 
<imagepath>` line a good looking image; it makes a world of 
difference.

-	`yaourt polybar` will get you the bar.
Same deal as i3, reading the config will make everything make a 
lot of sense. In the configuration, the bar is named "top", so 
`polybar top &` in your program launcher will launch it. 
`killall -q polybar` will do what it sounds like, which was 
useful for me to reload them, because `polybar -r top` was 
spawning more of them. Alternatively, you can reload i3 with 
mod+shift+r and it will execute polybar's launch.sh, which kills 
all bars before starting a new one. 

Useful things I've found on my quest for a good looking UI:

-	terminal.sexy - testing color schemes for zsh

-	nerdfonts.com - grabbing icons for workspace buttons and 
command line
In order to use these icons, install a nerd-patched font 
(mentioned above) and run `echo '\u<iconhex>'` in your command line. 
Then, you can copy the character with ctrl-shift-c and paste it 
into your text editor. Some configs will accept '\u<iconhex>' as 
a character, but polybar refused, so I'm mentioning it here.

Current issues, to be fixed as I figure it out: 

-	No mod+<left/right> workspace switching

-	Due to my laptop model, fn+f11/f12 doesn't change screen 
brightness. You can change it manually with 
`# echo <0-7500> /sys/class/backlight/intel_backlight/brightness`
if your hardware is intel. Maybe aspci_video0 otherwise?
For models of laptop that do not come preinstalled with windows 
8+, this shouldn't be necessary, from my understanding. 

-	The internal microphone activates gain automatically 
when the 
machine is launched, which makes the input horrible. Have to 
figure out how to change this with amixer, will likely go into 
Xresources or xinit.

For questions/pointers (since I doubt I will get many/am 
sympathetic to the cause) you can message /u/maelrawn_ on reddit or email me at jdelaura00@gmail.com
